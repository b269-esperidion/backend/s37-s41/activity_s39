const Course = require("../models/Course");

// Create a new course
// module.exports.addCourse = (reqBody) => {
// 	let newCourse = new Course({
// 		name: reqBody.name,
// 		description: reqBody.description,
// 		price: reqBody.price
// 	});
// 	return newCourse.save().then((course, error) => {
// 		// Course creation failed
// 		if (error) {
// 			return false;
// 		// Course creation successful
// 		} else {
// 			return true;
// 		};
// 	});
// };

module.exports.addCourse = (data) => {
	// User is an admin
	if (data.isAdmin) {
		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newCourse = new Course({
			name : data.course.name,
			description : data.course.description,
			price : data.course.price
		});
		// Saves the created object to our database
		return newCourse.save().then((course, error) => {
			// Course creation successful
			if (error) {
				return false;
			// Course creation failed
			} else {
				return true;
			};
		});
	// User is not an admin
	};
	let message = Promise.resolve("User must be Admin to access this!");
		return message.then((value) => {
			return {value};
		});
};
